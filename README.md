# dumbwaysdotid

B13 K1 Bootcamp DumbWays

# Requirements

This application is run with PHP 7.2

# Usage

the programs 1-5 are only run by **Command Line**



*  ## Program 1
$`php path/to/file/1.php`

example: 

$`php path/to/file/1.php`


*  ## Program 2
$`php path/to/file/2.php [TAGIHAN] [PEMBAYARAN]`

example: 

$`php path/to/file/2.php 110500 200000`


*  ## Program 3
$`php path/to/file/3.php [SIZE]`

example: 

$`php path/to/file/3.php 7`


*  ## Program 4
$`php path/to/file/4.php [JAM]`

example: 

$`php path/to/file/4.php 5`


*  ## Program 5
$`php path/to/file/5.php [NAMA] [NIM] [JUMLAH_HADIR] [NILAI_TUGAS] [NILAI_UTS] [NILAI_UAS]`

example: 

$`php path/to/file/5.php "DendiMP" "69696969" 14 80 85 90`

