<?php

// - tampilkan semua kategori
$q_cateqory = "SELECT * FROM categories";

// - tampilkan produk per kategori, field ditampilkan nama kategori, nama produk, stock
$q_productPerCategory = "SELECT c.name, p.name, p.stok FROM products p LEFT JOIN categories c on p.category_id = p.id WHERE p.category_id = ?";

// - tampilkan detail produk
$q_detProduct = "SELECT p.name, p.stok, p.deskripsi, c.name as category FROM products p LEFT JOIN categories c on p.category_id = p.id WHERE p.id = ?";


// - query post tambah kategori
$q_insCateqory = "INSERT INTO categories (id, name) VALUES (1, 'BEVERAGES')";
// - query post tambah produk
$q_insProduct = "INSERT INTO products (id, name, stok, deskripsi, category_id) VALUES (1, 'MANGO JUICE', 100, 1)";