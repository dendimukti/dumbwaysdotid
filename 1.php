<?php

    error_reporting(E_ERROR | E_PARSE);

    function check($dataKey, $word) {
        $result = [];
        foreach($dataKey as $value) {
            if (strpos($word, $value) !== false) {
                $result[$value] = true;
            } else {
                $result[$value] = false;
            }
        }
        return $result;
    }

    $dataKey = ['dumb','ways','the','best'];
    $word = 'dumbways';

    echo "\r\n";
    echo "DATA KEY \t: " . json_encode($dataKey) . " \r\n";
    echo "WORD \t\t: $word \r\n";
    echo "================================\r\n";

    $result = check($dataKey, $word);

    echo "RESULT \t\t: \r\n\r\n";
    print_r($result);
    echo "\r\n";

?>