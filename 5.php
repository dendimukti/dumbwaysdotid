<?php

    error_reporting(E_ERROR | E_PARSE);

    echo "=======================================================================================\r\n";
    echo "FORMAT TO ACCESS THIS PROGRAM: \r\n\r\n";
    echo "php path/to/file.php [NAMA] [NIM] [JUMLAH_HADIR] [NILAI_TUGAS] [NILAI_UTS] [NILAI_UAS] \r\n";
    echo "                 (str)  (str)      (int)         (int)        (int)       (int) \r\n\r\n";
    echo "=======================================================================================\r\n";
    
    $errors = "";
    if(!is_string($argv[1]))    $errors .= " - field NAMA is required as STRING format \r\n";
    if(!is_string($argv[2]))    $errors .= " - field NIM is required as STRING format \r\n";
    if(!is_numeric($argv[3]))    $errors .= " - field JUMLAH_HADIR is required as NUMERIC format \r\n";
    if(!is_numeric($argv[4]))    $errors .= " - field NILAI_TUGAS is required as NUMERIC format \r\n";
    if(!is_numeric($argv[5]))    $errors .= " - field NILAI_UTS is required as NUMERIC format \r\n";
    if(!is_numeric($argv[6]))    $errors .= " - field NILAI_UAS is required as NUMERIC format \r\n";

    if(!empty($errors)) {
        echo "ERRORS:\r\n";
        echo $errors;
        echo "=======================================================================================\r\n";
        die();
    }

    echo "INPUT:\t\t| \r\n";
    echo "=================\r\n";
    $nama = $argv[1];
    $nim = $argv[2];
    $jumlah_hadir = $argv[3];
    $nilai_tugas = $argv[4];
    $nilai_uts = $argv[5];
    $nilai_uas = $argv[6];
    $nilai_kehadiran = ($jumlah_hadir/14) * 100;

    echo "NAMA \t\t: $nama \r\n";
    echo "NIM \t\t: $nim \r\n";
    echo "JUMLAH_HADIR \t: $jumlah_hadir / 14 ($nilai_kehadiran) \r\n";
    echo "NILAI_TUGAS \t: $nilai_tugas \r\n";
    echo "NILAI_UTS \t: $nilai_uts \r\n";
    echo "NILAI_UAS \t: $nilai_uas \r\n";
    echo "=======================================================================================\r\n";
    echo "OUTPUT: \t| \r\n";
    echo "=================\r\n";
    $total = ($nilai_kehadiran * 0.1) + ($nilai_tugas * 0.2) + ($nilai_uts * 0.3) + ($nilai_uas * 0.4);
    
    $grade = "";
    if($total > 80) $grade = "A";
    else if($total > 70 && $total < 80) $grade = "B";
    else if($total > 60 && $total <= 70) $grade = "C";
    else if($total >= 50 && $total <= 60) $grade = "D";
    else if($total < 50) $grade = "E";

    if( $nilai_kehadiran * $nilai_tugas * $nilai_uts * $nilai_uas === 0) {
        $grade = "E";
    }
    echo "NAMA \t\t: $nama \r\n";
    echo "NIM \t\t: $nim \r\n";
    echo "NILAI_TOTAL \t: $total \r\n";
    echo "NILAI_GRADE \t: $grade \r\n";
    echo "=======================================================================================\r\n";
    
    echo "\r\n";
?>