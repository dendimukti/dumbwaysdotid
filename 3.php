<?php

    error_reporting(E_ERROR | E_PARSE);

    function drawImage($size) {
        if($size % 2 === 0)
            return "SIZE harus GANJIL \r\n";

        $result = "";
        $middle = ($size / 2) + 0.5;

        for($i = 1; $i <= $size; $i++) {
            if($i <= $middle) {
                for($x = 0; $x < $middle - $i; $x++){
                    $result .= "= ";
                }
            }else{
                for($x = 0; $x < $i - $middle; $x++){
                    $result .= "= ";
                }
            }

            if($i <= $middle) {
                for($x = 0; $x < 1 + (2*($i-1)); $x++){
                    $result .= "@ ";
                }
            }else{
                for($x = 0; $x < 1 + (2*($size - $i)); $x++){
                    $result .= "@ ";
                }
            }

            if($i <= $middle) {
                for($x = 0; $x < $middle - $i; $x++){
                    $result .= "= ";
                }
            }else{
                for($x = 0; $x < $i - $middle; $x++){
                    $result .= "= ";
                }
            }

            $result .= "\r\n";
        }

        return $result;
    }

    echo "=======================================================================================\r\n";
    echo "FORMAT TO ACCESS THIS PROGRAM: \r\n\r\n";
    echo "php path/to/file.php [SIZE] \r\n";
    echo "                     (int) \r\n";
    echo "==================================\r\n";
    echo "EXAMPLE: \r\n\r\n";
    echo "php path/to/file.php 7 \r\n";
    echo "=======================================================================================\r\n";
    
    $errors = "";
    if(!is_numeric($argv[1]))
        $errors .= " - field SIZE is required as NUMERIC format \r\n";
    if(is_numeric($argv[1])){
        if($argv[1] % 2 === 0)
            $errors .= " - SIZE value require as ODD value \r\n";
    }

    if(!empty($errors)) {
        echo "ERRORS:\r\n";
        echo $errors;
        echo "=======================================================================================\r\n";
        die();
    }

    echo "INPUT:\t\t| \r\n";
    $size = $argv[1];
    echo "=================\r\n";
    echo "SIZE \t\t: $size \r\n";
    echo "=======================================================================================\r\n";
    echo "OUTPUT: \t| \r\n";
    echo "=================\r\n\r\n";

    $result = drawImage($size);

    echo $result;
    echo "\r\n";
    echo "=======================================================================================\r\n";
    echo "\r\n";

?>