<?php

    error_reporting(E_ERROR | E_PARSE);

    function hitungParkir($jam) {
        $result = 0;
        for($i = 1; $i <= $jam; $i++) {
            if($i <= 3)
                $result += 2000;
            else
                $result += 1000;
            
            if($result === 10000)
                break;
        }

        return $result;
    }

    echo "=======================================================================================\r\n";
    echo "FORMAT TO ACCESS THIS PROGRAM: \r\n\r\n";
    echo "php path/to/file.php [JAM] \r\n";
    echo "                     (int) \r\n";
    echo "==================================\r\n";
    echo "EXAMPLE: \r\n\r\n";
    echo "php path/to/file.php 5 \r\n";
    echo "=======================================================================================\r\n";
    
    $errors = "";
    if(!is_numeric($argv[1]))
        $errors .= " - field JAM is required as NUMERIC format \r\n";

    if(!empty($errors)) {
        echo "ERRORS:\r\n";
        echo $errors;
        echo "=======================================================================================\r\n";
        die();
    }

    echo "INPUT:\t\t| \r\n";
    $jam = $argv[1];
    echo "=================\r\n";
    echo "JUMLAH JAM \t: $jam \r\n";
    echo "=======================================================================================\r\n";
    echo "OUTPUT: \t| \r\n";
    echo "=================\r\n";

    $result = hitungParkir($jam);

    echo "BIAYA \t\t: $result\r\n";
    echo "=======================================================================================\r\n";
    echo "\r\n";

?>