<?php

    error_reporting(E_ERROR | E_PARSE);

    function hitungKembalian($tagihan, $pembayaran) {
        $pecahan_uang = [50000, 20000, 10000, 5000];
        $total_kembalian = $pembayaran - $tagihan;

        $pecahan_kembalian = [];
        foreach($pecahan_uang as $lembar_uang) {
            $total_sisa = $total_kembalian % $lembar_uang;
            $pecahan_kembalian[$lembar_uang] = ($total_kembalian - $total_sisa) / $lembar_uang . " Lembar";
            $total_kembalian = $total_sisa;
        }
        $pecahan_kembalian[$total_kembalian] = "Disumbangkan";
        
        return $pecahan_kembalian;
    }

    echo "=======================================================================================\r\n";
    echo "FORMAT TO ACCESS THIS PROGRAM: \r\n\r\n";
    echo "php path/to/file.php [TAGIHAN] [PEMBAYARAN] \r\n";
    echo "                       (int)      (int) \r\n";
    echo "==================================\r\n";
    echo "EXAMPLE: \r\n\r\n";
    echo "php path/to/file.php 110500 200000 \r\n";
    echo "=======================================================================================\r\n";
    
    $errors = "";
    if(!is_numeric($argv[1]))
        $errors .= " - field TAGIHAN is required as NUMERIC format \r\n";
    if(!is_numeric($argv[2]))
        $errors .= " - field PEMBAYARAN is required as NUMERIC format \r\n";
    if(is_numeric($argv[1]) && is_numeric($argv[2])){
        if($argv[1] > $argv[2])
            $errors .= " - PEMBAYARAN should equal or greater than TAGIHAN \r\n";
    }

    if(!empty($errors)) {
        echo "ERRORS:\r\n";
        echo $errors;
        echo "=======================================================================================\r\n";
        die();
    }

    echo "INPUT:\t\t| \r\n";
    $tagihan = $argv[1];
    $pembayaran = $argv[2];
    echo "=================\r\n";
    echo "TAGIHAN \t: $tagihan \r\n";
    echo "PEMBAYARAN \t: $pembayaran \r\n";
    echo "=======================================================================================\r\n";
    echo "OUTPUT: \t| \r\n";
    echo "=================\r\n";

    $kembalian = hitungKembalian($tagihan, $pembayaran);
    echo "KEMBALIAN \t: " . ($pembayaran - $tagihan) . "\r\n\r\n";
    print_r($kembalian);
    echo "=======================================================================================\r\n";
    echo "\r\n";

?>